<?php

return [
    'options.1' => 'Suche auf dieser Domain neu starten',
    'options.2' => ':host ausblenden',
    'options.3' => '*.:domain ausblenden',
    'options.4' => 'Partnershop',
    'options.5' => 'anonym öffnen',
];
