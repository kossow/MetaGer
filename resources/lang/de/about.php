<?php

return [
    'head.1' => 'Über uns',
    'head.2' => 'Welche Vorteile bietet die Nutzung von MetaGer?',

    'list.1' => 'Datenschutz und Privatsphäre sind bei uns einfach und selbstverständlich: Fertig eingebaut und automatisch bei jeder Suche angewandt. <a href="/datenschutz/">Mehr dazu...</a>',
    'list.2' => 'Wir arbeiten nicht gewinnorientiert, wir sind ein <a href="/spende/">gemeinnütziger Verein:</a> Wir haben nicht das Ziel, uns durch Ihre Klicks und schon gar nicht durch Ihre Daten zu bereichern.',
    'list.3' => '<a href="https://de.wikipedia.org/wiki/MetaGer" target="_blank">MetaGer</a> ist primär eine <a href="https://de.wikipedia.org/wiki/Metasuchmaschine" target="_blank">META-Suchmaschine:</a> Wir fragen bis zu 50 Suchmaschinen ab. Damit können wir echte Vielfalt in den Ergebnissen liefern.',
    'list.4' => 'Wir bevorzugen in unseren Suchergebnissen nicht das, <a href="https://de.wikipedia.org/wiki/Filterblase" target="_blank">was viel angeklickt wird:</a> Auch dadurch erhalten Sie nicht nur den Mainstream, sondern Vielfältigkeit.',
    'list.5' => '<a href="http://blog.suma-ev.de/node/207" target="_blank">MetaGer ist seit 20 Jahren am Netz:</a> Unsere Erfahrung ist Ihr Vorteil - wir wissen was wir tun.',
    'list.6' => 'Aber auch wir sind nicht fehlerfrei: Wenn Ihnen bei uns Merkwürdiges begegnet: Bitte <a href="/kontakt/" target="_blank">kontaktieren Sie uns!</a> Wir nehmen Ihre Hinweise ernst: SIE sind uns das Wichtigste.',
];
