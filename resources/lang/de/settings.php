<?php

return [
    'head.1'          => 'Einstellungen',
    'head.2'          => 'Hier können Sie Ihr MetaGer anpassen: Nach Anklicken Ihrer gewünschten Einstellungen müssen Sie <a href="#unten">unten auf dieser Seite</a> wählen, ob Sie die Einstellungen dauerhaft speichern, oder nur einmalig setzen wollen.',

    'allgemein.1'     => 'Allgemein',
    'allgemein.2'     => 'Sprüche ausblenden',
    'allgemein.3'     => 'Links im gleichen Tab öffnen',
    'allgemein.4'     => 'Sprache auswählen:',
    'allgemein.5'     => 'Alle Sprachen',
    'allgemein.6'     => 'Deutsch',
    'allgemein.6_1'   => 'Englisch',
    'allgemein.7'     => 'Anzahl der Ergebnisse pro Seite:',
    'allgemein.8'     => 'Alle',

    'zeit.1'          => 'Maximale Suchzeit',
    'zeit.2'          => '1 Sekunde (Standard)',
    'zeit.3'          => '2 Sekunden',
    'zeit.4'          => '5 Sekunden',
    'zeit.5'          => '10 Sekunden',
    'zeit.6'          => '20 Sekunden',

    'suchmaschinen.1' => 'Suchmaschinen',
    'suchmaschinen.2' => '(alle abwählen)',
    'suchmaschinen.3' => '(alle an-/abwählen)',

    'speichern.1'     => 'Startseite für einmalige Nutzung generieren',
    'speichern.2'     => 'Einstellungen dauerhaft speichern',
    'speichern.3'     => 'Plugin mit diesen Einstellungen generieren',
    'speichern.4'     => 'Einstellungen zurücksetzen',

    'request'         => 'Mit welcher Methode soll MetaGer abgefragt werden',
];
