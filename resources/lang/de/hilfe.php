<?php

return [
    'achtung'                  => 'Achtung, da sich unsere Website in der aktiven Entwicklung befindet, kann es dazu kommen, dass sich immer wieder Änderungen an Aufbau und Funktion der Website ergeben. Wir versuchen stets die Hilfe schnellstmöglich den Änderungen entsprechend anzupassen, können jedoch nicht verhindern, dass es zu temporären Unstimmigkeiten in Teilen der Erklärungen kommt.',

    'title'                    => 'MetaGer - Hilfe',
    'einstellungen'            => 'Einstellungen',

    'dienste'                  => 'Dienste',

    'suchwortassoziator.title' => 'Suchwortassoziator',
    'suchwortassoziator.1'     => 'Als Hilfe für die Erschließung eines Begriffsumfelds haben wir den <a href="https://metager.de/klassik/asso/" target="_blank">MetaGer-Web-Assoziator</a> entwickelt. Sie finden das Tool auch direkt unter dem Reiter "Dienste". Gibt man in diesen ein Suchwort ein, welches dem zu untersuchenden Fachgebiet irgendwie nahe kommt, dann wird versucht, typische Fachbegriffe dieses Gebietes aus dem WWW zu extrahieren.',
    'suchwortassoziator.2'     => 'Beispiel: Sie möchten mehr über Zeckenbisse und deren Gefahren wissen, aber ihnen fallen die medizinischen Fachbegriffe für Erkrankungen aus diesem Bereich nicht mehr ein. Die Eingabe des Wortes "Zeckenbisse" in den Web-Assoziator liefert dann u.a. die Begriffe "Borreliose" und "fsme".',
    'suchwortassoziator.3'     => 'Da diese Assoziationsanalyse u.a. aus Web-Dokumenten selber gewonnen wird, ist sie sprachunabhängig; d.h. Sie können bei Eingabe deutscher Wörter Fachbegriffe aus beliebigen Sprachen gewinnen (und umgekehrt). Wenn Ihnen andererseits Assoziationsanalysen auffallen, die mit Hilfe Ihrer Fachkenntnisse besser sein könnten, dann zögern Sie bitte nicht, uns dieses samt Ihrem Verbesserungsvorschlag <a href="/kontakt/" target="_blank">über unser Kontaktformular</a> mitzuteilen.',

    'widget.title'             => 'MetaGer Widget',
    'widget.1'                 => 'Hierbei handelt es sich um einen Codegenerator, der es Ihnen ermöglicht, MetaGer in Ihre Webseite einzubinden. Sie können damit dann nach Belieben auf Ihrer eigenen Seite oder im Internet suchen lassen. Bei allen Fragen: <a href="/kontakt/" target="_blank">unser Kontaktformular</a>',

    'urlshort.title'           => 'URL-Verkürzer',
    'urlshort.1'               => 'Sie finden den <a href="https://metager.to/" target="_blank">URL-Verkürzer</a> auch direkt unter "Dienste". Wenn Sie einen extrem langen Link- oder Domainnamen haben, können Sie diesen hier in eine kurze und prägnante Form bringen. MetaGer sorgt dann zusammen mit Yourls für die Weiterleitung.',
    'dienste.kostenlos'        => 'Selbstverständlich sind all unsere Dienste kostenlos',
    'datenschutz.title'        => 'Anonymität und Datensicherheit',
    'datenschutz.1'            => 'Cookies, Session-IDs und IP-Adressen',
    'datenschutz.2'            => 'Nichts von alldem wird hier bei MetaGer gespeichert, aufgehoben oder sonst irgendwie verarbeitet. Weil wir diese Thematik für extrem wichtig halten, haben wir auch Möglichkeiten geschaffen, die Ihnen helfen können, hier ein Höchstmaß an Sicherheit zu erreichen: den MetaGer-TOR-Hidden-Service und unseren Proxyserver.',
    'datenschutz.3'            => 'Genauere Informationen dazu finden Sie unter der Überschrift "Dienste".',

    'tor.title'                => 'Tor-Hidden-Service',
    'tor.1'                    => 'Bei MetaGer werden schon seit vielen Jahren die IP-Adressen der Nutzer anonymisiert und nicht gespeichert. Nichtsdestotrotz sind diese Adressen auf dem MetaGer-Server sichtbar: wenn MetaGer also einmal kompromittiert sein sollte, dann könnte dieser Angreifer Ihre Adressen mitlesen und speichern. Um dem höchsten Sicherheitsbedürfnis entgegenzukommen, unterhalten wir eine MetaGer-Repräsentanz im Tor-Netzwerk: den MetaGer-TOR-hidden-Service - erreichbar über: <a href="/tor/" target="_blank">https://metager.de/tor/</a>. Für die Benutzung benötigen Sie einen speziellen Browser, den Sie etwa auf <a href="https://www.torproject.org/" target="_blank">https://www.torproject.org/</a> herunter laden können (Details siehe: <a href="http://forum.suma-ev.de/viewtopic.php?f=3&t=43" target="_blank">http://forum.suma-ev.de/viewtopic.php?f=3&t=43</a>).',
    'tor.2'                    => 'MetaGer erreichen Sie im Tor-Browser dann unter: http://b7cxf4dkdsko6ah2.onion .',

    'proxy.title'              => 'MetaGer Proxyserver',
    'proxy.1'                  => 'Um ihn zu verwenden, müssen Sie auf der MetaGer-Ergebnisseite nur auf den Link "anonym öffnen" rechts neben dem Ergebnislink klicken. Dann wird Ihre Anfrage an die Zielwebseite über unseren anonymisierenden Proxy-Server geleitet und Ihre persönlichen Daten bleiben weiterhin völlig geschützt. Wichtig: wenn Sie ab dieser Stelle den Links auf den Seiten folgen, bleiben Sie durch den Proxy geschützt. Sie können aber oben im Adressfeld keine neue Adresse ansteuern. In diesem Fall verlieren Sie den Schutz. Ob Sie noch geschützt sind, sehen Sie ebenfalls im Adressfeld. Es zeigt: https://proxy.suma-ev.de/?url=hier steht die eigentlich Adresse.',

    'allgemein.title'          => 'Allgemeine Einstellungen',
    'allgemein.1'              => 'Alle Einstellungen finden Sie unter dem Suchfokus "anpassen".',
    'allgemein.2'              => 'Der Farbtropfen links neben dem Suchfeld ermöglicht Ihnen eine individuelle Farbeinstellung für die Startseite.',
    'allgemein.3'              => 'Ein Plugin für die allermeisten Browser finden Sie <a href="/#plugin-modal" target="_blank">direkt</a> unter dem Suchfeld, wo Ihr Browser bereits voreingestellt sein sollte.',

    'suchfokus.title'          => 'Auswahl des Suchfokus',
    'suchfokus.1'              => 'Über dem Suchfeld finden Sie fünf Sucheinstellungen, die den meisten Anforderungen genügen ( "Web", "Bilder", "Nachrichten", "Wissenschaft" sowie "Produkte" ). Über den Button "anpassen" rechts daneben können Sie persönliche Feineinstellungen dazu vornehmen. Zuletzt entscheiden Sie über die Verwendung Ihrer Einstellungen. Sie finden ganz unten unter den Einstellungen drei Buttons: entweder benutzen Sie die Einstellung nur für eine Suche (hierfür können Sie auch ein Lesezeichen setzen), für eine dauerhafte Verwendung, oder Sie können dafür ein Plugin erstellen. MetaGer speichert Ihre Einstellungen dann im sogenannten "Local Storage" (des Browsers), hierfür benötigen Sie Javascript.',

    'sucheingabe'              => 'Sucheingabe',

    'stopworte.title'          => 'Stoppworte',
    'stopworte.1'              => 'Wenn Sie unter den MetaGer-Suchergebnissen solche ausschlie&szlig;en wollen, in denen bestimmte Worte (Ausschlussworte / Stopworte) vorkommen, dann erreichen Sie das, indem Sie diese Worte mit einem Minus versehen.',
    'stopworte.2'              => 'Beispiel: Sie suchen ein neues Auto, aber auf keinen Fall einen BMW. Ihre Eingabe lautet also: <div class="well well-sm">auto neu -bmw</div>',

    'mehrwortsuche.title'      => 'Mehrwortsuche',
    'mehrwortsuche.1'          => 'Bei einer Mehrwortsuche werden als Voreinstellung diejenigen Dokumente gesucht, in denen möglichst alle Worte vorkommen, oder solche, die denen möglichst nahe kommen. Die Suche nach mehreren Begriffen zeigt annähernd gleiche Ergebnisse mit oder ohne Verwendung von Anführungszeichen. Wenn Sie jedoch zum Beispiel ein längeres Zitat oder so etwas suchen, sollten Sie Anführungszeichen verwenden.',
    'mehrwortsuche.2'          => 'Beispiel: die Suche nach <div class="well well-sm">"in den öden Fensterhöhlen"</div> liefert viele Ergebnisse, aber spannend (und genauer) wird es bei der Suche <div class="well well-sm">Schiller "in den öden Fensterhöhlen"</div>',
    'grossklein.title'         => 'Gro&szlig;-/ Kleinschreibung',
    'grossklein.1'             => 'Gro&szlig;- und Kleinschreibung wird bei der Suche nicht unterschieden, es handelt sich um eine rein inhaltliche Suche.',
    'grossklein.2'             => 'Beispiel: die Suche nach <div class="well well-sm">gro&szlig;schreibung</div> liefert genau die gleichen Ergebnisse wie <div class="well well-sm">GRO&szlig;SCHREIBUNG</div>',

    'dienste'                  => 'Dienste',

    'suchwortassoziator.title' => 'Suchwortassoziator',
    'suchwortassoziator.1'     => 'Als Hilfe für die Erschließung eines Begriffsumfelds haben wir den <a href="https://metager.de/klassik/asso/" target="_blank">MetaGer-Web-Assoziator</a> entwickelt. Sie finden das Tool auch direkt unter dem Reiter "Dienste". Gibt man in diesen ein Suchwort ein, welches dem zu untersuchenden Fachgebiet irgendwie nahe kommt, dann wird versucht, typische Fachbegriffe dieses Gebietes aus dem WWW zu extrahieren.',
    'suchwortassoziator.2'     => 'Beispiel: Sie möchten mehr über Zeckenbisse und deren Gefahren wissen, aber ihnen fallen die medizinischen Fachbegriffe für Erkrankungen aus diesem Bereich nicht mehr ein. Die Eingabe des Wortes "Zeckenbisse" in den Web-Assoziator liefert dann u.a. die Begriffe "Borreliose" und "fsme".',
    'suchwortassoziator.3'     => 'Da diese Assoziationsanalyse u.a. aus Web-Dokumenten selber gewonnen wird, ist sie sprachunabhängig; d.h. Sie können bei Eingabe deutscher Wörter Fachbegriffe aus beliebigen Sprachen gewinnen (und umgekehrt). Wenn Ihnen andererseits Assoziationsanalysen auffallen, die mit Hilfe Ihrer Fachkenntnisse besser sein könnten, dann zögern Sie bitte nicht, uns dieses samt Ihrem Verbesserungsvorschlag <a href="/kontakt/" target="_blank">über unser Kontaktformular</a> mitzuteilen.',

    'widget.title'             => 'MetaGer Widget',
    'widget.1'                 => 'Hierbei handelt es sich um einen Codegenerator, der es Ihnen ermöglicht, MetaGer in Ihre Webseite einzubinden. Sie können damit dann nach Belieben auf Ihrer eigenen Seite oder im Internet suchen lassen. Bei allen Fragen: <a href="/kontakt/" target="_blank">unser Kontaktformular</a>',

    'urlshort.title'           => 'URL-Verkürzer',
    'urlshort.1'               => 'Sie finden den <a href="https://metager.to/" target="_blank">URL-Verkürzer</a> auch direkt unter "Dienste". Wenn Sie einen extrem langen Link- oder Domainnamen haben, können Sie diesen hier in eine kurze und prägnante Form bringen. MetaGer sorgt dann zusammen mit Yourls für die Weiterleitung.',
    'dienste.kostenlos'        => 'Selbstverständlich sind all unsere Dienste kostenlos',
    'datenschutz.title'        => 'Anonymität und Datensicherheit',
    'datenschutz.1'            => 'Cookies, Session-IDs und IP-Adressen',
    'datenschutz.2'            => 'Nichts von alldem wird hier bei MetaGer gespeichert, aufgehoben oder sonst irgendwie verarbeitet. Weil wir diese Thematik für extrem wichtig halten, haben wir auch Möglichkeiten geschaffen, die Ihnen helfen können, hier ein Höchstmaß an Sicherheit zu erreichen: den MetaGer-TOR-Hidden-Service und unseren Proxyserver.',
    'datenschutz.3'            => 'Genauere Informationen dazu finden Sie unter der Überschrift "Dienste".',

    'tor.title'                => 'Tor-Hidden-Service',
    'tor.1'                    => 'Bei MetaGer werden schon seit vielen Jahren die IP-Adressen der Nutzer anonymisiert und nicht gespeichert. Nichtsdestotrotz sind diese Adressen auf dem MetaGer-Server sichtbar: wenn MetaGer also einmal kompromittiert sein sollte, dann könnte dieser Angreifer Ihre Adressen mitlesen und speichern. Um dem höchsten Sicherheitsbedürfnis entgegenzukommen, unterhalten wir eine MetaGer-Repräsentanz im Tor-Netzwerk: den MetaGer-TOR-hidden-Service - erreichbar über: <a href="/tor/" target="_blank">https://metager.de/tor/</a>. Für die Benutzung benötigen Sie einen speziellen Browser, den Sie etwa auf <a href="https://www.torproject.org/" target="_blank">https://www.torproject.org/</a> herunter laden können (Details siehe: <a href="http://forum.suma-ev.de/viewtopic.php?f=3&t=43" target="_blank">http://forum.suma-ev.de/viewtopic.php?f=3&t=43</a>).',
    'tor.2'                    => 'MetaGer erreichen Sie im Tor-Browser dann unter: http://b7cxf4dkdsko6ah2.onion .',

    'proxy.title'              => 'MetaGer Proxyserver',
    'proxy.1'                  => 'Um ihn zu verwenden, müssen Sie auf der MetaGer-Ergebnisseite nur auf den Link "anonym öffnen" rechts neben dem Ergebnislink klicken. Dann wird Ihre Anfrage an die Zielwebseite über unseren anonymisierenden Proxy-Server geleitet und Ihre persönlichen Daten bleiben weiterhin völlig geschützt. Wichtig: wenn Sie ab dieser Stelle den Links auf den Seiten folgen, bleiben Sie durch den Proxy geschützt. Sie können aber oben im Adressfeld keine neue Adresse ansteuern. In diesem Fall verlieren Sie den Schutz. Ob Sie noch geschützt sind, sehen Sie ebenfalls im Adressfeld. Es zeigt: https://proxy.suma-ev.de/?url=hier steht die eigentlich Adresse.',
];
