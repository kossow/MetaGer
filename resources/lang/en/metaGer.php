<?php

return [
    "results.failed"         => "Unfortunately we have no results for your search",
    "settings.noneSelected"  => "Attention: you did not choose any search engine",
    "formdata.cantLoad"      => "Could not find suma-file",
    "formdata.noSearch"      => "Attention: you did not type in any search word/s. Please type search words and try again",
    "formdata.dartEurope"    => "Hint: you have activated Dart-Europe. Therefore the response time might be longer and is set to 10 sec",
    "formdata.hostBlacklist" => "Results of the following domains will not be shown: \":domain\"",
    "formdata.stopwords"     => "You have excluded results with the follwing words: \":stopwords\"",
    "formdata.phrase"        => "You are doing a string search: :phrase",
    "sitesearch.failed"      => "You intend to do a site search on :site. Unfortunately the choosen search engines do not support that. You can do a site search <a href=\":searchLink\">here</a> within the Web focus",
    "sitesearch.success"     => "You are doing a site search. Only resulte of the website <a href=\"http://:site\" target=\"_blank\">\":site\"</a> will be shown.",
];
