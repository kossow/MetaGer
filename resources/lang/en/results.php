<?php

return [
    'redirect' => '(with redirect)',
    'weiter'   => 'more',
    'zurueck'  => 'back',
];
