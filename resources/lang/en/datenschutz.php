<?php

return [
    'head'      => 'Privacy',

    'general.1' => 'For us, privacy is one of the most important goods on the internet. It needs to be preserved and is not to be used for profit. The following is a small summary of our approach.',

    'general.3' => 'Only free software with open source code is under control of any user.  In
all other cases the users have to <em>believe</em> what the maintainers tell about
what is going on inside.  If maintainers claim that they have strictly
implemented privacy and data protecion, users have to believe them.
MetaGer is free software under GNU-AGPLv3 Licence. It is available to
anyone at <a href="https://gitlab.metager3.de/open-source/MetaGer">https://gitlab.metager3.de/open-source/MetaGer</a>.',

    'policy.1'  => 'Our Approach/Policy:',
    'policy.2'  => 'Neither do we save your IP, nor the
<a href="https://en.wikipedia.org/wiki/Canvas_fingerprinting" target="_blank">"fingerprint" of your Browser</a> (which most likely could identify you as well).',

    'policy.5'  => 'We do not set cookies or use tracking-pixels or similar technology, to "track" our users (tracking = following a users digital movement on the internet).',
    'policy.6'  => 'Data transmission is only done encrypted via the https protocol.',
    'policy.7'  => 'We offer access via the anonymous TOR network, the <a href="http://forum.suma-ev.de/viewtopic.php?f=3&amp;t=43&amp;sid=c994b628153235dfef098ba6fea3d60e" target="_blank">MetaGer hidden service</a>.',

    'policy.9'  => 'Since access via the TOR-network seems complicated to many users and often slow, we offer a proxy server, to visit the result pages anonymously: By clicking on the link "open anonymously" your personal information will be protected on this click and even on all following clicks.',
    'policy.10' => 'We show as few advertisements as possible, clearly marking them, and financially rely on our users, their <a href="/en/spende/">Donations</a> and membership fees for <a href="http://suma-ev.de/en/" target="_blank">SUMA-EV</a>.',

    'policy.13' => 'Metager is operated and developed by the charitable german organisation <a href="http://suma-ev.de/en/" target="_blank">SUMA-EV</a> in cooperation with the <a href="http://www.uni-hannover.de/" target="_blank">Leibniz University Hanover</a>.',

    'policy.17' => 'Our Servers are located exclusively in germany. Therefore they are governed by the german privacy law, which is among the strictest world wide.',
    'policy.18' => 'Since the <a href="http://www.heise.de/newsticker/meldung/Bericht-US-Regierung-zapft-Kundendaten-von-Internet-Firmen-an-1884264.html" target="_blank">revealments of Edward Snowden in June 2013</a> numerous search engines claim that they protect their users\' privacy, because the users\' IP adresses are not saved. No matter how honorable these descriptions are - many search engines at least partially host their servers in the USA. Because these search engines are, <a href="http://de.wikipedia.org/wiki/USA_PATRIOT_Act" target="_blank">as per the Patriot Act and US law</a>, unprotected against the access of local authorities. Thus they cannot provide protection of personal data (even if they really try to).',

    'twitter'   => 'What others say about our privacy-concept on twitter:',
];
