<?php

return [
    "achtung"   => "Warning! For our website is in constant development there are continuous changes to the structure and functionality of our website. We try to update our help pages as quickly as possible, but can not prevent temporary mistakes.",

    "title"     => "MetaGer - FAQ",

    "faq.1.h"   => "What is MetaGer?",
    "faq.1.b"   => "MetaGer is primarily a <a href=\"https://en.wikipedia.org/wiki/Metasearch_engine\" target=\"_blank\">meta search engine</a>. Besides that MetaGer maintains a number of specialized crawlers and indexers of its own.",
    "faq.2.h"   => "Which search engines are meta-crawled by MetaGer?",
    "faq.2.b"   => "You can find a list at our \"customize\" menu on the MetaGer homepage.",
    "faq.3.h"   => "Why does MetaGer has no button \"search using all search engines\"?",
    "faq.3.b"   => "The reason is, that the requirements of a search are are often not suitable for this. For a search for scientific results for example it makes no sense to search with a product search engine.",
    "faq.4.h"   => "Why don't you metacrawl search engine XY anymore?",
    "faq.4.b"   => "If we do not query a search engine which we did query before, then this has technical or \"political\" reasons.",
    "faq.5.h"   => "One search engine does not answer anymore",
    "faq.5.b"   => "If you observe this it is probably a technical fault. Please email us in that case: <a href=\"mailto:office@suma-ev.de\">office@suma-ev.de</a>",
    "faq.6.h"   => "Inappropriate Content",
    "faq.6.b.1" => "> I found results with illegal content.",
    "faq.6.b.2" => "If you found content through MetaGer, which you think is illegal, you can email us at <a href=\"mailto:jugendschutz@metager.de\" target=\"_blank\">jugendschutz@metager.de</a>.",
    "faq.7.h"   => "Is it possible to integrate MetaGer into my own website?",
    "faq.7.b"   => "No problem! We have a <a href=\"/en/widget\" target=_blank>widget</a> that you can use free of charge.",
    "faq.8.h"   => "How can I register my homepage on MetaGer?",
    "faq.8.b"   => "You can't. MetaGer is a metasearch engine. If you want to propagate your website, you have to register at all the other search engines for that purpose.",
    "faq.9.h"   => "How does the MetaGer ranking work?",
    "faq.9.b"   => "You can check <a href=\"https://gitlab.metager3.de/open-source/MetaGer\" target=_blank>our source code</a> to find out.",
    "faq.10.h"  => "I can't use MetaGer with my browser X and operating system Y. I need help!",
    "faq.15.h"  => "How was MetaGer started, what is the story behind MetaGer?",
];
