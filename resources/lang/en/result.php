<?php

return [
    'options.1' => 'start a new search on this domain',
    'options.2' => 'hide :host',
    'options.3' => 'hide *.:domain',
    'options.4' => 'partnershop',
    'options.5' => 'open anonymously',
];
