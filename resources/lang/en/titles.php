<?php

return [
    "index"	=>	"MetaGer: Search and Find Savely, Protect Privacy",
    "impressum"	=>	"Site Notice - MetaGer",
    "about"	=>	"About Us - MetaGer",
    "team"	=>	"Team - MetaGer",
    "kontakt"	=>	"Contact - MetaGer",
    "spende"	=>	"Donation - MetaGer",
    "datenschutz"	=>	"Privacy - MetaGer",
    "hilfe"	=>	"Help - MetaGer",
    "widget"	=>	"MetaGer Widget",
    "settings"	=>	"Settings",
    "websearch"	=>	"Websearch-Widget - MetaGer",
    "sitesearch"	=>	"Sitesearch-Widget - MetaGer",
    "faq"	=>	"FAQ - MetaGer",
    "partnershops"	=>	"Partnershops - MetaGer",
    "languages.edit"	=>	"Edit language files - MetaGer"
];