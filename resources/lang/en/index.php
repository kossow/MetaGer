<?php

return [
    "foki.web"	=>	"Web",
    "foki.bilder"	=>	"Pictures",
    "foki.nachrichten"	=>	"News",
    "foki.wissenschaft"	=>	"Science",
    "foki.produkte"	=>	"Shopping",
    "foki.anpassen"	=>	"customize",
    "foki.angepasst"	=>	"customized",
    "design"	=>	"select personal theme",
    "conveyor"	=>	"Purchase at affiliate shops",
    "partnertitle"	=>	"Support MetaGer without any costs to you",
    "plugin"	=>	"Add the MetaGer-Plugin",
    "plugintitle"	=>	"add MetaGer to your browser",
    "sponsors.head.1"	=>	"Further Information",
    "sponsors.head.2"	=>	"Sponsored Links",
    "sponsors.woxikon"	=>	"<a href=\"/en/kontakt\" class=\"mutelink\" >This could be your link</a>",
    "sponsors.gutscheine"	=>	"<a href=\"/en/kontakt\" class=\"mutelink\" >Please contact us</a>",
    "sponsors.kredite"	=>	"<a href=\"/en/kontakt\" class=\"mutelink\">https://metager.de/en/kontakt</a>",
    "about.title"	=>	"About Us",
    "about.1.1"	=>	"Data protection & privacy",
    "about.1.2"	=>	"  are simple and natural for us.",
    "about.2.1"	=>	"We do not work for profit. We are a ",
    "about.2.2"	=>	"non-profit association. ",
    "placeholder"	=>	"MetaGer: secure search & find, protecting privacy",
    "plugin.head.1"	=>	"Add MetaGer to your Firefox",
    "plugin.head.2"	=>	"Add MetaGer to your Chrome",
    "plugin.head.3"	=>	"Add MetaGer to your Opera",
    "plugin.head.4"	=>	"Add MetaGer to your Internet Explorer",
    "plugin.head.5"	=>	"Add MetaGer to your Microsoft Edge",
    "plugin.head.6"	=>	"Add MetaGer to your Safari",
    "plugin.head.info"	=>	"(selected search preferences will be used)",
    "plugin.firefox.1"	=>	"Click on the magnifying glass with the small green \"+\" and then on \"Add MetaGer..\" (picture 1)",
    "plugin.firefox.2"	=>	"Now right click on the new MetaGer Logo in the List and select \"Make MetaGer the default search engine\" (picture2)",
    "plugin.firefox.3"	=>	"Use MetaGer as start page in :browser",
    "plugin.firefox.4"	=>	"Click top right in your browser on <span class=\"glyphicon glyphicon-menu-hamburger\" aria-hidden=\"true\"></span><span class=\"sr-only\">the three small horizontal lines</span> (menu) and open \"Preferences\"",
    "plugin.firefox.5"	=>	"Type in field \"Home Page\" => \"https://metager.de/en/\"",
    "plugin.chrome.1"	=>	"Click in your Chrome browser on <span class=\"glyphicon glyphicon-option-vertical\"></span> and in the next menu on \"Preferences\" to open the settings of your Chrome browser",
    "plugin.chrome.2"	=>	"Click in field \"Search\" on manage search engines",
    "plugin.chrome.3"	=>	"Now you will find an entry \"MetaGer\". Move your mouse over that entry and click on \"Standard\",",
    "plugin.chrome.4"	=>	"Set up MetaGer as start page in your browser",
    "plugin.chrome.5"	=>	"Click in your browser on<span class=\"glyphicon glyphicon-option-vertical\"></span> and open \"Preferences\"",
    "plugin.chrome.6"	=>	"In the field \"Start\" choose \"Open specific Page/s\" and then click on \"Choose Page/s\"",
    "plugin.chrome.7"	=>	"Type in https://metager.de/en/\" as URL at \"Add new page\"",
    "plugin.chrome.8"	=>	"Hint: Every Webpage in this field will be opened when you start your browser. You can delete entries by moving the mouse on it and click \"x\"",
    "plugin.opera.1"	=>	"Click on <a href=\"/\" target=\"_blank\">hier</a> to open MetaGer in a new TAB",
    "plugin.opera.2"	=>	"In that news TAB click with the right mouse button within the search field in the middle of the page",
    "plugin.opera.3"	=>	"Choose \"create search engine\" in the menu",
    "plugin.opera.4"	=>	"Click \"create\" within the popup",
    "plugin.opera.5"	=>	"(Unfortunately it is no loger possible to set up new search engines in the Opera browser as standard, but you might install the  open source browser <a href=\"https://www.mozilla.org/de/firefox/new/\" target=\"_blank\">Firefox</a>, who does support that)",
    "plugin.opera.6"	=>	"Set up MetaGer as start page in your browser",
    "plugin.opera.7"	=>	"Click in browser menu on \"Edit\" and them \"Preferences\"",
    "plugin.opera.8"	=>	"In the field \"Start\" choose \"Open specific Page\/s\" and then click on \"Choose Page\/s\"",
    "plugin.opera.9"	=>	"Type in https://metager.de/en/\" as URL at \"Add new page\"",
    "plugin.opera.10"	=>	"Click on OK",
    "plugin.IE.1"	=>	"Click <a href=\"javascript:window.external.addSearchProvider($('link[rel=search]').attr('href'));\">here</a> to add MetaGer as search engine",
    "plugin.IE.4"	=>	"Click top right in your browser on \"Extras\"",
    "plugin.IE.5"	=>	"Choose the menu \"manage Add-Ons\"",
    "plugin.IE.6"	=>	"Click in field \"Add-Ons\" on \"search provider\" and the on the right on \"MetaGer\"",
    "plugin.IE.7"	=>	"Coose button \"Standard\"",
    "plugin.IE.8"	=>	"Set up MetaGer as start page in your browser",
    "plugin.IE.9"	=>	"Click at top right in your browser on <span class=\"glyphicon glyphicon-cog\"></span> and open \"Internet Options\"",
    "plugin.IE.10"	=>	"If MetaGer should be your only startpage mark the existing text within the text field, and replace it by \"https://metager.de/en/\"",
    "plugin.IE.11"	=>	"Click on OK",
    "plugin.edge.1"	=>	"Click in your browser top right on Extras (",
    "plugin.edge.2"	=>	") and choose \"Preferences\"",
    "plugin.edge.3"	=>	"Scroll down and click on \"Show adavanced Preferences\"",
    "plugin.edge.4"	=>	"Scroll again down to topic \"within addresses\" and click on \"Change\"",
    "plugin.edge.5"	=>	"Choose \"MetaGer ...\" and click on \"Standard\"",
    "plugin.edge.6"	=>	"Set up MetaGer as start page in :browser",
    "plugin.edge.7"	=>	"Click at top right in your browser on <span class=\"glyphicon glyphicon-option-horizontal\"></span> and open \"Preferences\"",
    "plugin.edge.8"	=>	"Choose in the field \"Open\" the topic \"Specific pages\"",
    "plugin.edge.9"	=>	"Choose \"User defined\" within drop down menu",
    "plugin.edge.10"	=>	"Type \"https://metager.de/en/\" in field \"Webaddress\" and click on \"+\"",
    "plugin.edge.11"	=>	"Delete all entries which should not be opened at start (for example \"about:start\") by click on \"x\"",
    "plugin.safari.1"	=>	"Install the Safari-PlugIn <a href=\"http://www.opensearchforsafari.com/\" target=\"_blank\">OpenSearchforSafari</a>.",
    "plugin.safari.2"	=>	"Open MetaGer and add MetaGer by the OpenSearch-button within the Safari menu",
    "plugin.safari.3"	=>	"Delete (if you want) other search engines by clicking on \"X\"",
    "plugin.safari.4"	=>	"Now you might search with MetaGer via the OpenSearch-button (magnifying glass symbol)"
];