<?php

return [
    "head.1"	=>	"Settings",
    "head.2"	=>	"Here you can customize your MetaGer: After selecting your desired settings, you have to choose, whether you want to save the settings permanently, or just for once, <a href=\"#unten\">on the bottom of this page.</a>",
    "allgemein.1"	=>	"General",
    "allgemein.2"	=>	"disable quotes",
    "allgemein.3"	=>	"open links in same tab",
    "allgemein.4"	=>	"select language:",
    "allgemein.5"	=>	"all languages",
    "allgemein.6"	=>	"german",
    "allgemein.6_1"	=>	"english",
    "allgemein.7"	=>	"number of results per page:",
    "allgemein.8"	=>	"all",
    "zeit.1"	=>	"maximum search time",
    "zeit.2"	=>	"1 second (standard)",
    "zeit.3"	=>	"2 seconds",
    "zeit.4"	=>	"5 seconds",
    "zeit.5"	=>	"10 seconds",
    "zeit.6"	=>	"20 seconds",
    "suchmaschinen.1"	=>	"search engines",
    "suchmaschinen.2"	=>	"(deselect all)",
    "suchmaschinen.3"	=>	"(select / deselect all)",
    "speichern.1"	=>	"Create a startpage for one-time use",
    "speichern.2"	=>	"Save settings permanently",
    "speichern.3"	=>	"Generate plugin with these settings",
    "speichern.4"	=>	"Reset saved settings",
    "request"	=>	"Which method should query MetaGer (GET, POST)?"
];