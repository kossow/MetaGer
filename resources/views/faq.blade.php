@extends('layouts.subPages')

@section('title', $title )

@section('content')
<div class="alert alert-warning" role="alert">{!! trans('faq.achtung') !!}</div>
<h1>{!! trans('faq.title') !!}</h1>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.1.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.1.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.2.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.2.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.3.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.3.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.4.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.4.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.5.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.5.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.6.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.6.b.1') !!}</p>
  <p>{!! trans('faq.faq.6.b.2') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.7.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.7.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.8.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.8.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.9.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.9.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.10.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.10.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.11.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.11.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.12.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.12.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.13.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.13.b.1') !!}</p>
  <p>{!! trans('faq.faq.13.b.2') !!}</p>
  <p>{!! trans('faq.faq.13.b.3') !!}</p>
  <p>{!! trans('faq.faq.13.b.4') !!}</p>
  <p>{!! trans('faq.faq.13.b.5') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.14.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.14.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.15.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.15.b') !!}</p>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{!! trans('faq.faq.16.h') !!}</h3>
  </div>
  <div class="panel-body">
    <p>{!! trans('faq.faq.16.b') !!}</p>
  </div>
</div>
@endsection
